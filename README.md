# TaskQueue

任务队列

## 介绍

nodejs 任务队列, 针对请求、IO 操作或其他异步操作高并发削峰的解决方案

## 调试说明

1.  pnpm build（构建）
2.  pnpm example（示例）
3.  pnpm debug（调试源码）

## 用法介绍

### 安装依赖

`npm install task-queue-lib`
或
`yarn add task-queue-lib`
或
`pnpm install task-queue-lib`

### 引入

#### ESM

```javascript
import { TaskQueue } from "task-queue-lib";
```

#### CJS

```javascript
const { TaskQueue } = require("task-queue-lib");
```

#### 浏览器中

```html
<script src="./node_modules/task-queue-lib/dist/umd/index.js"></script>
<script>
  console.log(TaskQueue);
</script>
```

### 使用

#### 切片长度 maxLen

```javascript
const taskQueue = new TaskQueue({ maxLen: 10 });
```

#### 新建一个队列

```javascript
const taskQueue = new TaskQueue({ maxLen: 10 });`
`taskQueue.push([() => {}])
```

#### 单个队列中的函数，有几个就 push 几个

```javascript
taskQueue.push(syncFn.bind(null, "args"));
```

#### 某个队列中的函数全部执行完成后会触发后续操作

```javascript
taskQueue.push([() => {}]).then(console.log); // [ undefined ]
```

或

```javascript
taskQueue.on(taskQueue.defaultKey, console.log).push([() => {}]);
```

#### 队列索引，通过第二个参数分组，异步操作成组完成

```javascript
const fn = (params) => params;
taskQueue.push([fn.bind(this, "hello")], "task1");
taskQueue.push([fn.bind(this, "world")], "task1").then(console.log); // [ 'hello', 'world' ]
taskQueue.push([fn.bind(this, "world")], "task2").then(console.log); // [ 'world' ]
```

#### 删除前三个异步函数

```javascript
taskQueue.unshift(3);
```

#### 初始化当前队列

```javascript
taskQueue.clearQueue();
```

#### 队列步进函数和事件

当我们通过 maxLen 将 push 的队列进行拆分，可以监听队列的步进事件，比如队列长度为 10，maxLen 为 3，那么就会执行 4 次步进函数及事件。

```javascript
// 实例化TaskQueue对象时，传入函数stepCb可以收到队列每次执行的消息。此外通过使用on方法，可以监听队列的步进事件，事件命名规则是`${key}:${step}`，key是taskQueue.push的第二个参数，step是当前队列的步进值。

const stopFn = async () => {
  const stopQueue = new TaskQueue({
    maxLen: 1,
    stepCb: (data) => {
      // data类型参考：IStepCbParams
      console.log(data);
    },
  });
  const key = "key1";
  const queue = [
    () => Promise.resolve("hello"),
    () => Promise.resolve("1"),
    () => Promise.resolve("stop"),
    () => Promise.resolve("2"),
    () => Promise.reject("3"),
  ];
  stopQueue.push(queue, "key1");
  for (let i = 1; i < queue.length + 1; i++) {
    stopQueue.on(key + `:${i}`, (data) => {
      const isStop = data.step === 3;
      console.log(isStop);
      if (isStop) stopQueue.clearQueue(); // 停止队列后续操作
    });
  }
};
stopFn();
```
